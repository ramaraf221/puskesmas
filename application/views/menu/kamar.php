<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Data Kamar</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('Dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Kamar</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Daftar Kamar</h3>
					<button type="submit" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">Tambah Kamar</button>

					<div class="modal fade" id="modal-default">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Tambah Kamar</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="<?= base_url('menu/kamar'); ?>" method="post">
									<div class="modal-body">
										<div class="form-group">
											<label for="nokamar">No Kamar</label>
											<input type="text" class="form-control" id="nokamar" placeholder="No Kamar" name="nokamar">
										</div>
									</div>
									<div class="modal-footer justify-content-between">
										<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
										<button type="submit" class="btn btn-primary">Simpan</button>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No Kamar</th>
								<th>Status</th>
								<th style="width: 80px;" class="text-center">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($daftarkamar as $kamar) : ?>
								<tr>
									<td><?= $kamar['nm_kamar']; ?></td>
									<td><?= $kamar['status']; ?></td>
									<td class="text-center py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info" data-toggle="modal" data-target="#modalkamar-<?= $kamar['id_kamar']; ?>"><i class="fas fa-eye"></i></a>
											<a href="<?= base_url('Menu/hapuskamar/' . $kamar['id_kamar']); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
										</div>
									</td>
									<div class="modal fade" id="modalkamar-<?= $kamar['id_kamar']; ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Tambah Kamar</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form action="<?= base_url('menu/updatekamar/' . $kamar['id_kamar']); ?>" method="post">
													<div class="modal-body">
														<div class="form-group">
															<label for="nokamar">No Kamar</label>
															<input type="text" class="form-control" id="nokamar" placeholder="No Kamar" name="nokamar" value="<?= $kamar['nm_kamar']; ?>">
														</div>
													</div>
													<div class="modal-footer justify-content-between">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary">Simpan</button>
													</div>
												</form>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>No Kamar</th>
								<th>Status</th>
								<th style="width: 80px;" class="text-center">Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.row (main row) -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
