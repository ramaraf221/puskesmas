<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Data Dokter</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('Dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Dokter</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Daftar Dokter</h3>
					<button type="submit" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">Tambah Dokter</button>

					<div class="modal fade" id="modal-default">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Tambah Dokter</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="<?= base_url('Menu/dokter'); ?>" method="post">
									<div class="modal-body">
										<div class="form-group">
											<label for="namadokter">Nama Dokter</label>
											<input type="text" class="form-control" id="namadokter" placeholder="Nama Dokter" name="namadokter">
										</div>
										<div class="form-group">
											<label for="nohp">Nohp</label>
											<input type="text" class="form-control" id="nohp" placeholder="Nohp" name="nohp">
										</div>
										<div class="form-group">
											<label for="Alamat">Alamat</label>
											<input type="text" class="form-control" id="Alamat" placeholder="Alamat" name="alamat">
										</div>
									</div>
									<div class="modal-footer justify-content-between">
										<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
										<button type="submit" class="btn btn-primary">Simpan</button>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nama Dokter</th>
								<th>Nohp</th>
								<th>Alamat</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($daftardokter as $dokter) : ?>
								<tr>
									<td><?= $dokter['nm_dokter']; ?></td>
									<td><?= $dokter['nohp']; ?></td>
									<td><?= $dokter['alamat']; ?></td>
									<td class="text-center py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info" data-toggle="modal" data-target="#modalkamar-<?= $dokter['id_dokter']; ?>"><i class="fas fa-eye"></i></a>
											<a href="<?= base_url('Menu/hapusdokter/' . $dokter['id_dokter']); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
										</div>
									</td>
									<div class="modal fade" id="modalkamar-<?= $dokter['id_dokter']; ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Update Dokter</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form action="<?= base_url('menu/updatedokter/' . $dokter['id_dokter']); ?>" method="post">
													<div class="modal-body">
														<div class="form-group">
															<label for="namadokter">Nama Dokter</label>
															<input type="text" class="form-control" id="namadokter" placeholder="Nama Dokter" name="namadokter" value="<?= $dokter['nm_dokter']; ?>">
														</div>
														<div class="form-group">
															<label for="nohp">Nohp</label>
															<input type="text" class="form-control" id="nohp" placeholder="Nohp" name="nohp" value="<?= $dokter['nohp']; ?>">
														</div>
														<div class="form-group">
															<label for="nohp">Nohp</label>
															<input type="text" class="form-control" id="nohp" placeholder="Nohp" name="alamat" value="<?= $dokter['alamat']; ?>">
														</div>
													</div>
													<div class="modal-footer justify-content-between">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary">Simpan</button>
													</div>
												</form>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Nama Dokter</th>
								<th>Nohp</th>
								<th>Alamat</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.row (main row) -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
