<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Data Pasien Rawat Inap</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('Dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Pasien Rawat Inap</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Daftar Pasien Rawat Inap</h3>
					<button type="submit" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">Tambah Pasien</button>

					<div class="modal fade" id="modal-default">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Tambah Pasien Rawat Inap</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="<?= base_url('menu/pasienrawatinap'); ?>" method="post">
									<div class="modal-body">
										<div class="form-group">
											<label for="namapasien">Nama Pasien</label>
											<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien">
										</div>
										<div class="form-group">
											<label for="sakit">Sakit</label>
											<input type="text" class="form-control" id="sakit" placeholder="Sakit" name="sakit">
										</div>
										<div class="form-group">
											<label for="umur">Umur</label>
											<input type="text" class="form-control" id="umur" placeholder="Umur" name="umur">
										</div>
										<div class="form-group">
											<label for="date">Tanggal Masuk</label>
											<input type="date" class="form-control" id="date" placeholder="Tanggal Masuk" name="tanggalmasuk">
										</div>
										<div class="form-group">
											<label for="date">Tanggal Keluar</label>
											<input type="date" class="form-control" id="date" placeholder="Tanggal Keluar" name="tanggalkeluar">
										</div>
										<div class="form-group">
											<label for="exampleSelectBorder">Dokter</label>
											<select class="custom-select form-control-border" id="exampleSelectBorder" name="dokter">
												<option selected>=- Pilih Dokter -=</option>
												<?php foreach ($datadokter as $dokter) : ?>
													<option value="<?= $dokter['id_dokter']; ?>"><?= $dokter['nm_dokter']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="form-group">
											<label for="exampleSelectBorder">No Kamar</label>
											<select class="custom-select form-control-border" id="exampleSelectBorder" name="kamar">
												<option selected>=- Pilih Kamar -=</option>
												<?php foreach ($datakamar as $kamar) : ?>
													<option value="<?= $kamar['id_kamar']; ?>"><?= $kamar['nm_kamar']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="modal-footer justify-content-between">
										<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
										<button type="submit" class="btn btn-primary">Simpan</button>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nama Pasien</th>
								<th>Sakit</th>
								<th>Umur</th>
								<th>Tanggal Masuk</th>
								<th>Tanggal Keluar</th>
								<th>Nomor Kamar</th>
								<th>Nama Dokter</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($datarawatinap as $pasien) : ?>
								<tr>
									<td><?= $pasien['nm_pasien']; ?></td>
									<td><?= $pasien['sakit']; ?></td>
									<td><?= $pasien['umur']; ?></td>
									<td><?= $pasien['startdate']; ?></td>
									<td><?= $pasien['enddate']; ?></td>
									<td><?= $pasien['nm_kamar']; ?></td>
									<td><?= $pasien['nm_dokter']; ?></td>
									<td class="text-center py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info" data-toggle="modal" data-target="#modal-default-<?= $pasien['id_rawatinap']; ?>"><i class="fas fa-eye"></i></a>
											<a href="<?= base_url('Menu/hapuspasienrawatinap/' . $pasien['id_rawatinap']); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
										</div>
									</td>
									<div class="modal fade" id="modal-default-<?= $pasien['id_rawatinap']; ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Tambah Pasien Rawat Inap</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form action="<?= base_url('menu/updatepasienrawatinap/' . $pasien['id_rawatinap']); ?>" method="post">
													<div class="modal-body">
														<div class="form-group">
															<label for="namapasien">Nama Pasien</label>
															<input type="text" class="form-control" id="namapasien" placeholder="Nama Pasien" name="namapasien" value="<?= $pasien['nm_pasien']; ?>">
														</div>
														<div class="form-group">
															<label for="sakit">Sakit</label>
															<input type="text" class="form-control" id="sakit" placeholder="Sakit" name="sakit" value="<?= $pasien['sakit']; ?>">
														</div>
														<div class="form-group">
															<label for="umur">Umur</label>
															<input type="text" class="form-control" id="umur" placeholder="Umur" name="umur" value="<?= $pasien['umur']; ?>">
														</div>
														<div class="form-group">
															<label for="date">Tanggal Masuk</label>
															<input type="date" class="form-control" id="date" placeholder="Tanggal Masuk" name="tanggalmasuk" value="<?= $pasien['startdate']; ?>">
														</div>
														<div class="form-group">
															<label for="date">Tanggal Keluar</label>
															<input type="date" class="form-control" id="date" placeholder="Tanggal Keluar" name="tanggalkeluar" value="<?= $pasien['enddate']; ?>">
														</div>
														<div class="form-group">
															<label for="exampleSelectBorder">Dokter</label>
															<select class="custom-select form-control-border" id="exampleSelectBorder" name="dokter">
																<?php foreach ($datadokter as $dokter) : ?>
																	<?php if ($dokter['id_dokter'] == $pasien['id_dokter']) {
																	?>
																		<option selected value="<?= $pasien['id_dokter'] ?>"><?= $pasien['nm_dokter']; ?></option>
																	<?php
																	} ?>
																	<option value="<?= $dokter['id_dokter']; ?>"><?= $dokter['nm_dokter']; ?></option>
																<?php endforeach; ?>
															</select>
														</div>
														<div class="form-group">
															<label for="exampleSelectBorder">No Kamar</label>
															<select class="custom-select form-control-border" id="exampleSelectBorder" name="kamar">

																<?php foreach ($datakamar as $kamar) : ?>
																	<?php if ($kamar['id_kamar'] == $pasien['id_kamar']) {
																	?>
																		<option selected value="<?= $pasien['id_kamar'] ?>"><?= $pasien['nm_kamar']; ?></option>
																	<?php
																	} ?>
																	<option value="<?= $pasien['id_rawatinap']; ?>"><?= $kamar['nm_kamar']; ?></option>
																<?php endforeach; ?>
															</select>
														</div>
													</div>
													<div class="modal-footer justify-content-between">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary">Simpan</button>
													</div>
												</form>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Nama Pasien</th>
								<th>Sakit</th>
								<th>Umur</th>
								<th>Tanggal Masuk</th>
								<th>Tanggal Keluar</th>
								<th>Nomor Kamar</th>
								<th>Nama Dokter</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.row (main row) -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
