<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Data Akun Karyawan</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="<?= base_url('Dashboard'); ?>">Home</a></li>
						<li class="breadcrumb-item active">Karyawan</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="card-header">
					<h3 class="card-title">Daftar Karyawan</h3>
					<button type="submit" class="btn btn-primary float-right" data-toggle="modal" data-target="#modal-default">Tambah Karyawan</button>

					<div class="modal fade" id="modal-default">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Tambah Karyawan</h4>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<form action="<?= base_url('Menu/akunpegawai'); ?>" method="post">
									<div class="modal-body">
										<div class="form-group">
											<label for="namadokter">Nama</label>
											<input type="text" class="form-control" id="namadokter" placeholder="Nama Karyawan" name="nama">
										</div>
										<div class="form-group">
											<label for="nohp">Username</label>
											<input type="text" class="form-control" id="nohp" placeholder="Username" name="username">
										</div>
										<div class="form-group">
											<label for="Alamat">Password</label>
											<input type="password" class="form-control" id="Alamat" placeholder="Password" name="password">
										</div>
										<div class="form-group">
											<label for="exampleSelectBorder">Role</label>
											<select class="custom-select form-control-border" id="exampleSelectBorder" name="role">
												<option selected>=- Pilih Role -=</option>
												<?php foreach ($rolekaryawan as $role) : ?>
													<option value="<?= $role['id_role']; ?>"><?= $role['role']; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="modal-footer justify-content-between">
										<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
										<button type="submit" class="btn btn-primary">Simpan</button>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Username</th>
								<th>Role</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($akunpegawai as $pegawai) : ?>
								<tr>
									<td><?= $pegawai['nama']; ?></td>
									<td><?= $pegawai['username']; ?></td>
									<td><?= $pegawai['role']; ?></td>
									<td class="text-center py-0 align-middle">
										<div class="btn-group btn-group-sm">
											<a href="#" class="btn btn-info" data-toggle="modal" data-target="#modalkamar-<?= $pegawai['id_user']; ?>"><i class="fas fa-eye"></i></a>
											<a href="<?= base_url('Menu/hapuspegawai/' . $pegawai['id_user']); ?>" class="btn btn-danger"><i class="fas fa-trash"></i></a>
										</div>
									</td>
									<div class="modal fade" id="modalkamar-<?= $pegawai['id_user']; ?>">
										<div class="modal-dialog">
											<div class="modal-content">
												<div class="modal-header">
													<h4 class="modal-title">Update Karyawan</h4>
													<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<form action="<?= base_url('menu/updatepegawai/' . $pegawai['id_user']); ?>" method="post">
													<div class="modal-body">
														<div class="form-group">
															<label for="namadokter">Nama Karyawan</label>
															<input type="text" class="form-control" id="namadokter" placeholder="Nama Karyawan" name="nama" value="<?= $pegawai['nama']; ?>">
														</div>
														<div class="form-group">
															<label for="namadokter">Username</label>
															<input type="text" class="form-control" id="namadokter" placeholder="Username" name="username" value="<?= $pegawai['username']; ?>">
														</div>
														<div class="form-group">
															<label for="password">Password</label>
															<input type="password" class="form-control" id="password" placeholder="Password" name="password">
														</div>
														<div class="form-group">
															<label for="exampleSelectBorder">Role</label>
															<select class="custom-select form-control-border" id="exampleSelectBorder" name="role">
																<option selected>=- Pilih Role -=</option>
																<?php foreach ($rolekaryawan as $role) : ?>
																	<option value="<?= $role['id_role']; ?>"><?= $role['role']; ?></option>
																<?php endforeach; ?>
															</select>
														</div>
													</div>
													<div class="modal-footer justify-content-between">
														<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
														<button type="submit" class="btn btn-primary">Simpan</button>
													</div>
												</form>
											</div>
											<!-- /.modal-content -->
										</div>
										<!-- /.modal-dialog -->
									</div>
								</tr>
							<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Nama</th>
								<th>Username</th>
								<th>Role</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
				<!-- /.card-body -->
			</div>
			<!-- /.row (main row) -->
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>