<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Menu extends CI_Model
{
	public function tambahkamar()
	{
		$data = [
			'nm_kamar' => $this->input->post('nokamar'),
			'id_status_kamar' => 2,
		];
		$this->db->insert('tb_kamar', $data);
	}

	public function daftarkamar()
	{
		return $this->db->select('*')->from('tb_kamar')->join('tb_status_kamar', 'tb_status_kamar.id_status_kamar = tb_kamar.id_status_kamar')->get()->result_array();
	}

	public function tambahdokter()
	{
		$data = [
			'nm_dokter' => $this->input->post('namadokter'),
			'nohp' => $this->input->post('nohp'),
			'alamat' => $this->input->post('alamat'),
		];
		$this->db->insert('tb_dokter', $data);
	}

	public function daftardokter()
	{
		return $this->db->select('*')->from('tb_dokter')->get()->result_array();
	}

	public function tambahpasienrawatinap()
	{
		$data = [
			'nm_pasien' => $this->input->post('namapasien'),
			'sakit' => $this->input->post('sakit'),
			'umur' => $this->input->post('umur'),
			'startdate' => $this->input->post('tanggalmasuk'),
			'enddate' => $this->input->post('tanggalkeluar'),
			'id_kamar' => $this->input->post('kamar'),
			'id_dokter' => $this->input->post('dokter'),
		];
		$this->db->insert('tb_rawatinap', $data);

		$data = [
			'id_status_kamar' => 1,
		];
		$this->db->where('id_kamar', $this->input->post('kamar'));
		$this->db->update('tb_kamar', $data);
	}

	public function daftarpasien()
	{
		return $this->db->select('*')->from('tb_rawatinap')->join('tb_kamar', 'tb_kamar.id_kamar = tb_rawatinap.id_kamar')->join('tb_dokter', 'tb_dokter.id_dokter = tb_rawatinap.id_dokter')->get()->result_array();
	}

	public function ubahstatuskamar($id)
	{
		$data = [
			'id_status_kamar' => 2
		];
		$this->db->where('id_kamar', $id);
		$this->db->update('tb_kamar', $data);
	}

	public function akunpegawai()
	{
		return $this->db->select('*')->from('tb_user')->join('tb_role', 'tb_role.id_role = tb_user.id_role')->get()->result_array();
	}

	public function role()
	{
		return $this->db->select('*')->from('tb_role')->get()->result_array();
	}
}
