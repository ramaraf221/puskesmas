<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_Dashboard extends CI_Model
{

	public function totalpasien()
	{
		return $this->db->select('*')->from('tb_rawatinap')->count_all_results();
	}

	public function totaldokter()
	{
		return $this->db->select('*')->from('tb_dokter')->count_all_results();
	}

	public function totalkamar()
	{
		return $this->db->select('*')->from('tb_kamar')->count_all_results();
	}

	public function totalpegawai()
	{
		return $this->db->select('*')->from('tb_user')->count_all_results();
	}
}
