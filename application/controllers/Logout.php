<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{
	public function index()
	{
		$this->session->unset_userdata('id_role');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('id_user');
		redirect('Auth');
	}
}
