<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');

		if (!empty($this->session->userdata('id_user'))) {
			redirect('Dashboard');
		}
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == false) {
			$this->load->view('auth/login');
		} else {
			$username = $this->input->post('username');
			$search = $this->db->get_where('tb_user', ['username' => $username])->row_array();
			if ($search) {
				if (password_verify($this->input->post('password'), $search['password'])) {
					$data = [
						'id_user' => $search['id_user'],
						'username' => $username,
						'id_role' => $search['id_role']
					];
					$this->session->set_userdata($data);
					redirect('Dashboard');
				} else {
					redirect('Auth');
				}
			} else {
				redirect('Auth');
			}
		}
	}
}
