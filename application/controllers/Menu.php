<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('M_Menu', 'menu');

		if (empty($this->session->userdata('id_user'))) {
			redirect('Auth');
		}
	}

	public function pasienrawatinap()
	{
		$this->form_validation->set_rules('namapasien', 'Namapasien', 'required');
		$this->form_validation->set_rules('sakit', 'Sakit', 'required');
		$this->form_validation->set_rules('umur', 'Umur', 'required');
		$this->form_validation->set_rules('tanggalmasuk', 'Tanggalmasuk', 'required');
		$this->form_validation->set_rules('tanggalkeluar', 'Tanggalkeluar', 'required');
		$this->form_validation->set_rules('dokter', 'Dokter', 'required');
		$this->form_validation->set_rules('kamar', 'Kamar', 'required');
		if ($this->form_validation->run() == false) {
			$data = [
				'datadokter' => $this->menu->daftardokter(),
				'datakamar' => $this->menu->daftarkamar(),
				'datarawatinap' => $this->menu->daftarpasien(),
			];
			$this->load->view('layout_menu/header');
			$this->load->view('layout_menu/sidebar');
			$this->load->view('menu/pasienrawatinap', $data);
			$this->load->view('layout_menu/footer');
		} else {
			$this->menu->tambahpasienrawatinap();
			redirect('Menu/pasienrawatinap');
		}
	}

	public function hapuspasienrawatinap($id)
	{
		$this->db->where('id_rawatinap', $id);
		$this->db->delete('tb_rawatinap');

		$cari = $this->db->get_where('tb_rawatinap', ['id_rawatinap' => $id])->row_array();
		$this->menu->ubahstatuskamar($cari['id_kamar']);
		redirect('Menu/pasienrawatinap');
	}

	public function updatepasienrawatinap($id)
	{
		$this->form_validation->set_rules('namapasien', 'Namapasien', 'required');
		$this->form_validation->set_rules('sakit', 'Sakit', 'required');
		$this->form_validation->set_rules('umur', 'Umur', 'required');
		$this->form_validation->set_rules('tanggalmasuk', 'Tanggalmasuk', 'required');
		$this->form_validation->set_rules('tanggalkeluar', 'Tanggalkeluar', 'required');
		if ($this->form_validation->run() == false) {
			redirect('Menu/pasienrawatinap');
		} else {

			$data = [
				'nm_pasien' => $this->input->post('namapasien'),
				'sakit' => $this->input->post('sakit'),
				'umur' => $this->input->post('umur'),
				'startdate' => $this->input->post('tanggalmasuk'),
				'enddate' => $this->input->post('tanggalkeluar'),
				'id_kamar' => $this->input->post('kamar'),
				'id_dokter' => $this->input->post('dokter')
			];
			$this->db->where('id_rawatinap', $id);
			$this->db->update('tb_rawatinap', $data);
			redirect('Menu/pasienrawatinap');
		}
	}

	public function dokter()
	{
		$this->form_validation->set_rules('namadokter', 'Namadokter', 'required');
		$this->form_validation->set_rules('nohp', 'nohp', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		if ($this->form_validation->run() == false) {
			$data = [
				'daftardokter' => $this->menu->daftardokter(),
			];
			$this->load->view('layout_menu/header');
			$this->load->view('layout_menu/sidebar');
			$this->load->view('menu/dokter', $data);
			$this->load->view('layout_menu/footer');
		} else {
			$this->menu->tambahdokter();
			redirect('Menu/dokter');
		}
	}

	public function hapusdokter($id)
	{
		$this->db->where('id_dokter', $id);
		$this->db->delete('tb_dokter');
		redirect('Menu/dokter');
	}

	public function updatedokter($id)
	{
		$this->form_validation->set_rules('namadokter', 'Namadokter', 'required');
		$this->form_validation->set_rules('nohp', 'Nohp', 'required');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required');
		if ($this->form_validation->run() == false) {
			redirect('Menu/dokter');
		} else {
			$data = [
				'nm_dokter' => $this->input->post('namadokter'),
				'nohp' => $this->input->post('nohp'),
				'alamat' => $this->input->post('alamat')
			];
			$this->db->where('id_dokter', $id);
			$this->db->update('tb_dokter', $data);
			redirect('Menu/dokter');
		}
	}

	public function kamar()
	{
		$this->form_validation->set_rules('nokamar', 'Nokamar', 'required');
		if ($this->form_validation->run() == false) {
			$data = [
				'daftarkamar' => $this->menu->daftarkamar(),
			];
			$this->load->view('layout_menu/header');
			$this->load->view('layout_menu/sidebar');
			$this->load->view('menu/kamar', $data);
			$this->load->view('layout_menu/footer');
		} else {
			$this->menu->tambahkamar();
			redirect('Menu/kamar');
		}
	}

	public function hapuskamar($id)
	{
		$this->db->where('id_kamar', $id);
		$this->db->delete('tb_kamar');
		redirect('Menu/kamar');
	}

	public function updatekamar($id)
	{
		$this->form_validation->set_rules('nokamar', 'Nokamar', 'required');
		if ($this->form_validation->run() == false) {
			redirect('Menu/kamar');
		} else {
			$data = [
				'nm_kamar' => $this->input->post('nokamar'),
			];
			$this->db->where('id_kamar', $id);
			$this->db->update('tb_kamar', $data);
			redirect('Menu/kamar');
		}
	}

	public function akunpegawai()
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required');
		if ($this->form_validation->run() == false) {

			$data = [
				'akunpegawai' => $this->menu->akunpegawai(),
				'rolekaryawan' => $this->menu->role(),
			];
			$this->load->view('layout_menu/header');
			$this->load->view('layout_menu/sidebar');
			$this->load->view('menu/pegawai', $data);
			$this->load->view('layout_menu/footer');
		} else {
			$data = [
				'nama' => $this->input->post('nama'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'id_role' => $this->input->post('role')
			];
			$this->db->insert('tb_user', $data);
			redirect('Menu/akunpegawai');
		}
	}

	public function hapuspegawai($id)
	{
		$this->db->where('id_user', $id);
		$this->db->delete('tb_user');
		redirect('Menu/akunpegawai');
	}

	public function updatepegawai($id)
	{
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('role', 'Role', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() == false) {
			redirect('Menu/akunpegawai');
		} else {
			$data = [
				'nama' => $this->input->post('nama'),
				'username' => $this->input->post('username'),
				'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),
				'id_role' => $this->input->post('role'),
			];
			$this->db->where('id_user', $id);
			$this->db->update('tb_user', $data);
			redirect('Menu/akunpegawai');
		}
	}
}
