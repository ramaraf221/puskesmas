<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_Dashboard', 'dash');

		if (empty($this->session->userdata('id_user'))) {
			redirect('Auth');
		}
	}

	public function index()
	{
		$data = [
			'totalpasien' => $this->dash->totalpasien(),
			'totaldokter' => $this->dash->totaldokter(),
			'totalkamar' => $this->dash->totalkamar(),
			'totalpegawai' => $this->dash->totalpegawai(),
		];
		$this->load->view('layout_dashboard/header');
		$this->load->view('layout_dashboard/sidebar');
		$this->load->view('menu/dashboard', $data);
		$this->load->view('layout_dashboard/footer');
	}
}
